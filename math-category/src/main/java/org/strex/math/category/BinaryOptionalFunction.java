package org.strex.math.category;

import com.sun.istack.internal.NotNull;
import org.strex.math.category.function.BinaryPureFunction;
import org.strex.math.category.function.UnaryPureFunction;

public final class BinaryOptionalFunction<A1, A2, R> {

    private final BinaryPureFunction<A1, A2, R> function;

    public BinaryOptionalFunction(BinaryPureFunction<A1, A2, R> function) {
        this.function = function;
    }

    /**
     * Applicative functor action
     * @param arg2
     * @return
     */
    public UnaryOptionalFunction<A1, R> apply(OptionalValue<A2> arg2) {
        if (arg2.isNull()) {
            return new UnaryOptionalFunction<A1, R>(null);
        }
        UnaryPureFunction<A1, R> unaryPureFunction = arg1 -> {
            return function.apply(arg1, arg2.get());
        };
        return new UnaryOptionalFunction<A1, R>(unaryPureFunction);
    }

    public static <A1, A2, R> UnaryPureFunction<A1, UnaryPureFunction<A2, R>> curry(@NotNull BinaryPureFunction<A1, A2, R> function) {
        return arg1 -> {
            return arg2 -> {
                return function.apply(arg1, arg2);
            };
        };
    }
}
