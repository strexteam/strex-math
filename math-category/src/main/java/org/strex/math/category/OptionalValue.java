package org.strex.math.category;

import org.strex.math.category.function.UnaryPureFunction;

public final class OptionalValue<T> {

    private final T value;

    private OptionalValue(T value) {
        this.value = value;
    }

    public static <T> OptionalValue<T> of(T value) {
        return new OptionalValue<T>(value);
    }

    public static <T> OptionalValue<T> empty() {
        return new OptionalValue<T>(null);
    }

    public T get() {
        return value;
    }

    public boolean isNull() {
        return value == null;
    }

    /**
     * Functor action
     * @param pureUnaryPureFunction
     * @param <R>
     * @return
     */
    public <R> OptionalValue<R> map(UnaryPureFunction<T, R> pureUnaryPureFunction) {
        if (isNull()) {
            return OptionalValue.empty();
        }
        return OptionalValue.of(pureUnaryPureFunction.apply(value));
    }

    /**
     * Monad action
     * @param pureUnaryPureFunction
     * @param <R>
     * @return
     */
    public <R> OptionalValue<R> bind(UnaryPureFunction<T, OptionalValue<R>> pureUnaryPureFunction) {
        if (isNull()) {
            return OptionalValue.empty();
        }
        return pureUnaryPureFunction.apply(value);
    }
}
