package org.strex.math.category;

import com.sun.istack.internal.NotNull;
import org.strex.math.category.function.BinaryPureFunction;
import org.strex.math.category.function.PolyPureFunction;
import org.strex.math.category.function.UnaryPureFunction;

public final class PolyOptionalFunction<A1, A2, A3, R> {

    private final PolyPureFunction<A1, A2, A3, R> function;

    public PolyOptionalFunction(PolyPureFunction<A1, A2, A3, R> function) {
        this.function = function;
    }

    public BinaryOptionalFunction<A1, A2, R> apply(OptionalValue<A3> arg3) {
        if (arg3.isNull()) {
            return new BinaryOptionalFunction<A1, A2, R>(null);
        }
        BinaryPureFunction<A1, A2, R> binaryPureFunction = (arg1, arg2) -> {
            return function.apply(arg1, arg2, arg3.get());
        };
        return new BinaryOptionalFunction<A1, A2, R>(binaryPureFunction);
    }

    public static <A1, A2, A3, R> UnaryPureFunction<A1, BinaryPureFunction<A2, A3, R>> curry(@NotNull PolyPureFunction<A1, A2, A3, R> function) {
        return arg1 -> {
            return (arg2, arg3) -> {
                return function.apply(arg1, arg2, arg3);
            };
        };
    }
}
