package org.strex.math.category;

import org.strex.math.category.function.UnaryPureFunction;

public final class UnaryOptionalFunction<A1, R> {

    private final UnaryPureFunction<A1, R> function;

    public UnaryOptionalFunction(UnaryPureFunction<A1, R> function) {
        this.function = function;
    }

    /**
     * Applicative functor action
     * @param arg1
     * @return
     */
    public OptionalValue<R> apply(OptionalValue<A1> arg1) {
        if (arg1.isNull()) {
            return OptionalValue.empty();
        }
        return OptionalValue.of(function.apply(arg1.get()));
    }
}
