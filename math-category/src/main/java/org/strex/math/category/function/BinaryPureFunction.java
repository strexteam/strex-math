package org.strex.math.category.function;

/**
 * A functional interface (callback) that computes a value based on multiple input values.
 * @param <A1> the type of the first argument value
 * @param <A2> the type of the second argument value
 * @param <R> the result type
 */
public interface BinaryPureFunction<A1, A2, R> {

    /**
     * Calculate a value based on the input values.
     * @param arg1 the first value
     * @param arg2 the second value
     * @return the result value
     */
    R apply(A1 arg1, A2 arg2);
}
