package org.strex.math.category.function;

public interface UnaryDirtyFunction<A1, R> {

    R apply(A1 arg1);
}
