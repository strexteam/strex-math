/**
 * Copyright 2016 Strex.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 */

package org.strex.math.category;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.strex.math.category.function.BinaryPureFunction;
import org.strex.math.category.function.UnaryPureFunction;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


class FunctorTest {

    UnaryPureFunction<Integer, Double> squareRootFunction;
    BinaryPureFunction<Double, Boolean, Integer> cleverRoundingFunction;

    OptionalValue<Integer> optionalSixteenValue = OptionalValue.of(16);
    OptionalValue<Integer> optionalZeroValue = OptionalValue.of(0);
    OptionalValue<Integer> optionalEmptyValue = OptionalValue.empty();

    UnaryPureFunction<Integer, OptionalValue<Double>> hundredDivisionDirtyFunction;

    @BeforeEach
    void init() {

        // defining pure functions
        squareRootFunction = Math::sqrt;
        cleverRoundingFunction = (arg1, arg2) -> {
            if (arg2) {
                return new Double(Math.floor(arg1)).intValue();
            } else {
                return new Double(Math.ceil(arg1)).intValue();
            }
        };

        // defining optional variables
        optionalSixteenValue = OptionalValue.of(16);
        optionalZeroValue = OptionalValue.of(0);

        // defining dirty functions
        hundredDivisionDirtyFunction = arg1 -> {
            if (arg1 == 0) {
                return OptionalValue.empty();
            }
            return OptionalValue.of((double) (64 / arg1));
        };
    }

    @Test
    @DisplayName("Functor test")
    void functorTest() {

        final OptionalValue<Double> optionalResultSquareRootResult = optionalSixteenValue.map(squareRootFunction);
        assertEquals((double) optionalResultSquareRootResult.get(), 4.0);
        final OptionalValue<Double> optionalEmptyResultSquareRootResult = optionalEmptyValue.map(squareRootFunction);
        assertTrue(optionalEmptyResultSquareRootResult.isNull());
    }

    @Test
    @DisplayName("Applicative functor test")
    void applicativeFunctorTest() {

        UnaryOptionalFunction<Integer, Double> optionalSquareRootFunction = new UnaryOptionalFunction<>(squareRootFunction);
        final OptionalValue<Double> optionalApplicativeResultSquareRootResult = optionalSquareRootFunction.apply(optionalSixteenValue);

        BinaryOptionalFunction<Double, Boolean, Integer> optionalRoundingFunction = new BinaryOptionalFunction<>(cleverRoundingFunction);
        final OptionalValue<Integer> optionalCleverRoundingResult = optionalRoundingFunction.apply(OptionalValue.of(true)).apply(OptionalValue.of(10.5));
    }

    @Test
    @DisplayName("Monad test")
    void monadTest() {

        // TODO: 17.12.2016 need to known how to check generic value types, is it really possible?
//        final OptionalValue<OptionalValue<<Double>> optionalInOptionalResult = optionalSixteenValue.map(hundredDivisionDirtyFunction);

        final OptionalValue<Double> correctResult = optionalSixteenValue.bind(hundredDivisionDirtyFunction);
        assertEquals((double)correctResult.get(), 4);
        final OptionalValue<Double> emptyResult = optionalZeroValue.bind(hundredDivisionDirtyFunction);
        assertTrue(emptyResult.isNull());
    }
}
