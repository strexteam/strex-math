package org.strex.math.category.function;

/**
 * A functional interface that takes a value and returns another value, possibly with a
 * different type
 *
 * @param <A1> the type of the argument value
 * @param <R> the type of the output value
 */
public interface UnaryPureFunction<A1, R> {
    /**
     * Apply some calculation to the input value and return some other value.
     * @param arg1 the input value
     * @return the output value
     */
    R apply(A1 arg1);
}
